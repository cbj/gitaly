# options for analysis running
run:
  # timeout for analysis, e.g. 30s, 5m, default is 1m
  timeout: 10m
  modules-download-mode: readonly

# list of useful linters could be found at https://github.com/golangci/awesome-go-linters
linters:
  disable-all: true
  enable:
    - bidichk
    - deadcode
    - errcheck
    - exportloopref
    - depguard
    - forbidigo
    - gci
    # We use both gofmt and gofumpt because gofumpt doesn't seem to be linting
    # for simplifications, while gofmt does.
    - gofmt
    - gofumpt
    - goimports
    - gosimple
    - govet
    - ineffassign
    - makezero
    - megacheck
    - misspell
    - noctx
    - nolintlint
    - revive
    - rowserrcheck
    - sqlclosecheck
    - staticcheck
    - structcheck
    - stylecheck
    - thelper
    - unconvert
    - unused
    - varcheck
    - wastedassign

linters-settings:
  depguard:
    list-type: blacklist
    include-go-root: true
    packages-with-error-message:
      - io/ioutil: "ioutil is deprecated starting with Go 1.16"
  forbidigo:
    forbid:
      # Tests and code which use timing-based setups have repeatedly resulted
      # in flaky tests and are considered a code smell. Tests should be
      # rewritten to use deterministic timing sources like tickers. Using the
      # following functions is thus disallowed. and a code smell.
      - ^context.WithDeadline$
      - ^context.WithTimeout$
      # Tests should always use `testhelper.Context()`: this context has
      # special handling for feature flags which allows us to assert that
      # they're tested as expected.
      - ^context.Background$
      - ^context.TODO$
      # Tests should not set the bare environment functions, but instead use
      # `t.Setenv()` or `testhelper.Unsetenv()`. These functions have sanity
      # checks to verify we don't use `t.Parallel()` when setting envvars.
      - ^os.Setenv$
      - ^os.Unsetenv$
  revive:
    rules:
      - name: context-as-argument
        arguments:
          # The context should always be first, except in our testing packages.
          allowTypesBefore: "*testing.T,*testing.B,testing.TB"
  stylecheck:
    # ST1000 checks for missing package comments. We don't use these for most
    # packages, so let's disable this check.
    checks: [ "all", "-ST1000" ]
  thelper:
    test:
      # The following linter would check whether we always call `t.Helper()` in
      # functions that are not the top-level testcase. While this is nice in
      # theory, in practice it would also impact e.g. usecases like
      # `testhelper.NewFeatureSets(...).Run(t, testWithFeatures)`. This isn't
      # really what we want, so we just leave these as disabled for the time
      # being.
      begin: false
    benchmark:
      begin: false
    tb:
      begin: false

issues:
  exclude-use-default: false
  exclude-rules:
    - linters:
        - forbidigo
      # This fine thing excludes all paths which don't end with "_test.go".
      path: "^([^_]|_([^t]|t([^e]|e([^s]|s([^t]|t([^\\.]|\\.([^g]|g[^o])))))))*$"
    - linters:
        - errcheck
      text: "Error return value of `[^`]+.(Close|Serve)` is not checked"
  # Maximum issues count per one linter. Set to 0 to disable. Default is 50.
  max-issues-per-linter: 0
  # Maximum count of issues with the same text. Set to 0 to disable. Default is 3.
  max-same-issues: 0
